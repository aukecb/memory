import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by aukem on 27-Apr-20.
 */
public class Button {
    JButton[] bt = new JButton[16];
    Images image = new Images();
    Score score = new Score();
    int buttonsClicked=1;
    int selected1, selected2;
    Boolean pause = false;

    public JButton newButton( int i) {
        bt[i] = new JButton();
        bt[i].setPreferredSize(new Dimension(100, 100));
//        bt[i].setHorizontalAlignment(bt[i].RIGHT);
        bt[i].setIcon(null);
        if(!pause){
            bt[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(!pause){
                        if(trackButtonsClicked() ==1){
                            selected2 = i;
                            bt[i].setIcon(image.getImage(i));
                            Timer timer = new Timer(1000, new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    if(!score.checkImages(selected1,selected2,buttonsClicked)){
                                        bt[selected1].setIcon(null);
                                        bt[selected2].setIcon(null);
                                    }
                                }
                            });
                            timer.setRepeats(false);
                            timer.start();
                            pause = true;
                        }else{
                            selected1 = i;
                            bt[i].setIcon(image.getImage(i));
                        }
                    }
                }
            });
        }


    return bt[i];
    }


    public int trackButtonsClicked(){
        if(buttonsClicked == 1){
            buttonsClicked = 0;

        }else{
            buttonsClicked += 1;
        }
        System.out.println(buttonsClicked);
        return buttonsClicked;
    }
}
