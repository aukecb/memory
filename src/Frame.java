import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by aukem on 27-Apr-20.
 */
public class Frame {
    JFrame frame = new JFrame("Memory");
    JPanel panel = new JPanel();
    JPanel menuPanel = new JPanel();
    JPanel gamePanel = new JPanel();
    JLabel scoreLabel = new JLabel();
    Button button = new Button();
    ArrayList randNumbers = new ArrayList();
    int score = 0;
    public Frame(){
        panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
        menuPanel.setLayout(new FlowLayout());
        gamePanel.setLayout(new GridLayout(4,4,1,1));
        for(int j=0; j<=15;j++){
            randNumbers.add(j);
        }
        System.out.println(randNumbers);
        Collections.shuffle(randNumbers);
        System.out.println(randNumbers);
        for(int i=0; i<16; i++){
            gamePanel.add(button.newButton((int)randNumbers.get(i)));
        }
        scoreLabel.setText("Score: "+score);
        menuPanel.add(scoreLabel);
        panel.add(menuPanel);
        panel.add(gamePanel);

        frame.add(panel);
        frame.setSize(500,500);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    public void setScore(int score){
        this.score = score;
    }

}
